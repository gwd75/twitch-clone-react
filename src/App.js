import "./App.css";
import Header from "./Components/Header/Header";
import Sidebar from "./Components/Sidebar/Sidebar";
import Games from "./Components/Games/Games";
import TopStreams from "./Components/TopStreams/TopStreams";
import Live from "./Components/Live/Live";
import GameStreams from "./Components/GameStreams/GameStreams";
import Resultat from "./Components/Resultat/Resultat";
import Erreur from "./Components/Erreur/Erreur";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

function App() {
  return (
    <Router forceRefresh={true}>
      <div className="App">
        <Header />
        <Sidebar />
        <Routes>
          <Route exact path='/' element={<Games />}/>
          <Route exact path='/top-streams' element={<TopStreams />}/>
          <Route exact path='/live/:slug' element={<Live />}/>
          <Route exact path='/game/:slug' element={<GameStreams />}/>
          <Route exact path='/resultats/' element={<Erreur />}/>
          <Route exact path='/resultats/:slug' element={<Resultat />}/>
        </Routes>
      </div>
    </Router>
  );
}

export default App;
