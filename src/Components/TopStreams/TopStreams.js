import React, { useEffect, useState } from "react";
import api from "../../api";
import { Link } from "react-router-dom";
import "./TopStreams.scss";

export default function TopStreams() {
  const [topChannels, setTopChannels] = useState([]);

  useEffect(() => {
    const fetchTopStreams = async () => {
      const result = await api.get("https://api.twitch.tv/helix/streams");
      let dataArray = result.data.data;
      // console.log(dataArray);
      let gameIDs = dataArray.map((stream) => {
        return stream.game_id;
      });
      let userIDs = dataArray.map((stream) => {
        return stream.user_id;
      });
      // console.log(gameIDs, userIDs);

      let baseUrlGames = "https://api.twitch.tv/helix/games?";
      let baseUrlUsers = "https://api.twitch.tv/helix/users?";
      let queryParamsGames = "";
      let queryParamsUsers = "";
      gameIDs.map((id) => {
        return (queryParamsGames = queryParamsGames + `id=${id}&`);
      });
      userIDs.map((id) => {
        return (queryParamsUsers = queryParamsUsers + `id=${id}&`);
      });

      let finalUrlGames = baseUrlGames + queryParamsGames;
      let finalUrlUsers = baseUrlUsers + queryParamsUsers;

      let gamesNames = await api.get(finalUrlGames);
      let getUsers = await api.get(finalUrlUsers);

      let gamesNameArray = gamesNames.data.data;
      let usersArray = getUsers.data.data;

      let finalArray = dataArray.map((stream) => {
        stream.gameName = "";
        // stream.truePic = "";
        stream.login = "";

        gamesNameArray.forEach((name) => {
          usersArray.forEach((user) => {
            if (stream.user_id === user.id && stream.game_id === name.id) {
              //   stream.truePic = user.profile_image_url;
              stream.gameName = name.name;
              stream.login = user.login;
            }
          });
        });
        let newUrl = stream.thumbnail_url
          .replace("{width}", "320")
          .replace("{height}", "180");
        stream.thumbnail_url = newUrl;
        return stream;
      });
      setTopChannels(finalArray);
    };
    fetchTopStreams();
  }, []);
  console.log(topChannels);
  return (
    <div>
      <h1 className="titreGames">Streams les plus populaires</h1>
      <div className="flexAccueil">
        {topChannels.map((channel, index) => {
          return (
            <div className="carteStream" key={index}>
              <img
                src={channel.thumbnail_url}
                alt={channel.user_name}
                className="imgCarte"
              />
              <div className="cardBodyStream">
                <h5 className="titreCartesStream">{channel.user_name}</h5>
                <p className="txtStream">Jeu : {channel.gameName}</p>
                <p className="txtStream viewers">
                  Viewers : {channel.viewer_count}
                </p>
                <Link
                  className="lien"
                  to={{ pathname: `/live/${channel.login}` }}
                >
                  <div className="btnCarte">Regarder {channel.user_name}</div>
                </Link>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}
