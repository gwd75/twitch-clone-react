import React, { useState, useEffect } from "react";
import api from "../../api";
import { Link } from "react-router-dom";
import "./Games.scss";

export default function Games() {
  const [games, setGames] = useState([]);

  useEffect(() => {
    const fetchGames = async () => {
      const result = await api.get("https://api.twitch.tv/helix/games/top");
      // console.log(result);

      let dataArray = result.data.data;
      let finalArray = dataArray.map((game) => {
        let newUrl = game.box_art_url
          .replace("{width}", "250")
          .replace("{height}", "300");
        game.box_art_url = newUrl;
        return game;
      });
      setGames(finalArray);
    };

    fetchGames();
  }, []);
  // console.log(games);
  return (
    <div>
      <h1 className="titreGames">Jeux les plus populaires</h1>
      <div className="flexAccueil">
        {games.map((game, index) => {
          // console.log(game.id);
          return (
            <div className="carteGames" key={index}>
              <img
                src={game.box_art_url}
                alt={game.name}
                className="imgCarte"
              />
              <div className="cardBodyGames">
                <h5 className="titreCarteGames">{game.name}</h5>
                <Link
                  className="lien"
                  to={{
                    pathname: "game/" + game.name,
                    // state: { id: game.id},
                  }}
                  state={{id: game.id}}
                >
                  <div className="btnCarte">Voir {game.name}</div>
                </Link>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}
