import React, { useEffect, useState } from "react";
import ReactTwitchEmbedVideo from "react-twitch-embed-video";
import api from "../../api";
import { useParams } from "react-router";
import "./Live.scss";

export default function Live() {
  let { slug } = useParams();
  // console.log(slug);
  const [infoStream, setInfoStream] = useState([]);
  const [infoGame, setInfoGame] = useState([]);
  const [streamerOnline, setStreamerOnline] = useState(false)

  useEffect(() => {
    const fetchData = async () => {
      const result = await api.get(
        `https://api.twitch.tv/helix/streams?user_login=${slug}`
      );
      // console.log(result);
      if (result.data.data.length === 0) {
        setInfoStream({ title: "Le streamer est offline !" });
        setStreamerOnline(false)
      } else {
        let gameId = result.data.data.map((gameid) => {
          return gameid.game_id;
        });

        const resultGameName = await api.get(
          `https://api.twitch.tv/helix/games?id=${gameId}`
        );
        // console.log(resultGameName);
        let gameName = resultGameName.data.data.map((gamename) => {
          return gamename.name;
        });

        setInfoGame(gameName);
        setInfoStream(result.data.data[0]);
        setStreamerOnline(true)
      }
    };
    fetchData();
  }, [slug]);
  // console.log(infoStream,infoGame);
  return (
    <div className="containerDecale">
      <ReactTwitchEmbedVideo height="754" width="100%" channel={slug} />
      <div className="contInfo">
        <div className="titreStream">{infoStream.title}</div>

        <div className="viewer"> {streamerOnline ? `Viewers : ${infoStream.viewer_count}` : ''}</div>
        <div className="infoGame">
            {streamerOnline ? `Streamer : ${infoStream.user_name}, Langue : ${infoStream.language}` : ''}
        </div>
        <div className="gameName">{streamerOnline ? `Jeu : ${infoGame}` : ''}</div>
      </div>
    </div>
  );
}
