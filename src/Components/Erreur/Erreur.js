import React from 'react'
import '../Resultat/Resultat.scss'

export default function Erreur() {
    return (
        <div className="containerDecaleResultats">
            <h4>Aucun résultat , <br/>
                Vérifiez l'orthographe est bonne ou si le streamer existe bien !
            </h4>
        </div>
    )
}
