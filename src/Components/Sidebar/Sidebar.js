import React, { useEffect, useState } from "react";
import api from "../../api";
import { Link } from "react-router-dom";
import "./Sidebar.scss";

export default function Sidebar() {
  const [topStreams, setTopStreams] = useState([]);

  useEffect(() => {
    const fetchTopStreams = async () => {
      const result = await api.get("https://api.twitch.tv/helix/streams");
      let dataArray = result.data.data;
      // console.log(dataArray);
      let gameIDs = dataArray.map((stream) => {
        return stream.game_id;
      });
      let userIDs = dataArray.map((stream) => {
        return stream.user_id;
      });
      // console.log(gameIDs, userIDs);

      let baseUrlGames = "https://api.twitch.tv/helix/games?";
      let baseUrlUsers = "https://api.twitch.tv/helix/users?";
      let queryParamsGames = "";
      let queryParamsUsers = "";
      gameIDs.map((id) => {
        return (queryParamsGames = queryParamsGames + `id=${id}&`);
      });
      userIDs.map((id) => {
        return (queryParamsUsers = queryParamsUsers + `id=${id}&`);
      });

      let finalUrlGames = baseUrlGames + queryParamsGames;
      let finalUrlUsers = baseUrlUsers + queryParamsUsers;

      let gamesNames = await api.get(finalUrlGames);
      let getUsers = await api.get(finalUrlUsers);

      let gamesNameArray = gamesNames.data.data;
      let usersArray = getUsers.data.data;

      let finalArray = dataArray.map((stream) => {
        stream.gameName = "";
        stream.truePic = "";
        stream.login = "";

        gamesNameArray.forEach((name) => {
          usersArray.forEach((user) => {
            if (stream.user_id === user.id && stream.game_id === name.id) {
              stream.truePic = user.profile_image_url;
              stream.gameName = name.name;
              stream.login = user.login;
            }
          });
        });
        return stream;
      });
      setTopStreams(finalArray.slice(0, 8));
    };
    fetchTopStreams();
  }, []);
  return (
    <div className="sidebar">
      <h2 className="titre-sidebar">Chaines recommandées</h2>
      <ul className="listeStream">
        {topStreams.map((stream, index) => {
          return (
              <Link className='lien' key={index} to={{pathname: `/live/${stream.login}`}}>
            <li className="containerFlexSidebar" key={index}>
              <img
                src={stream.truePic}
                alt={stream.user_name}
                className="profilePic"
              />
              <div className="streamUser">{stream.user_name}</div>
              <div className="viewerCountRight">
                <div className="redDot"></div>
                <div>{stream.viewer_count}</div>
              </div>
              <div className="gameNameSidebar">{stream.gameName}</div>
            </li>
          </Link>
            );
        })}
      </ul>
    </div>
  );
}
