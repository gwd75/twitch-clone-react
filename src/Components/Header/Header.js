import React, { useState, useEffect } from "react";
import logoTwitch from "./IconeTwitch.svg";
import logoSearch from "./Search.svg";
import logoMenu from "./MenuIco.svg";
import Croix from "./Croix.svg";
import { Link } from "react-router-dom";
import "./Header.scss";

export default function Header() {
  const [showMenu, setShowMenu] = useState(false);
  const [smallScreen, setSmallScreen] = useState(false);
  const [searchInput, setSearchInput] = useState("");

  useEffect(() => {
    const mediaQuery = window.matchMedia("(max-width: 900px");
    mediaQuery.addListener(handleMediaQueryChange);
    handleMediaQueryChange(mediaQuery);
    return () => {
      mediaQuery.removeListener(handleMediaQueryChange);
    };
  });

  const handleMediaQueryChange = (mediaQuery) => {
    if (mediaQuery.matches) {
      setSmallScreen(true);
    } else {
      setSmallScreen(false);
    }
  };

  const toggleMenuRes = () => {
    setShowMenu(!showMenu);
  };

  const hideMenu = () => {
    if (showMenu) {
      setShowMenu(!showMenu);
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
  }

  const handleKeyPress = (e) => {
    setSearchInput(e.target.value)
  }
  return (
    <div>
      <nav className="headerTop">
        {(showMenu || !smallScreen) && (
          <ul className="listeMenu">
            <li onClick={hideMenu} className="liensNav">
              <Link className="lien" to="/">
                <img src={logoTwitch} alt="Logo Twitch" className="logo" />
              </Link>
            </li>
            <li onClick={hideMenu} className="liensNav">
              <Link className="lien" to="/">
                Top Games
              </Link>
            </li>
            <li onClick={hideMenu} className="liensNav">
              <Link className="lien" to="/top-streams">
                Top Streams
              </Link>
            </li>
            <li className="liensNav">
              <form className="formSubmit" onSubmit={handleSubmit}>
                <input
                  required
                  value={searchInput}
                  onChange={(e) => handleKeyPress(e)}
                  type="text"
                  className="inputRecherche"
                />
                <Link
                  className="lien"
                  to={{ pathname: `/resultats/${searchInput}` }}
                >
                  <button type="submit">
                    <img
                      src={logoSearch}
                      alt="icone Loupe"
                      className="logoLoupe"
                    />
                  </button>
                </Link>
              </form>
            </li>
          </ul>
        )}
      </nav>
      <div className="menu-responsive-btn">
        <img
          src={showMenu ? Croix : logoMenu}
          alt="icone menu responsive"
          className="menuIco"
          onClick={toggleMenuRes}
        />
      </div>
    </div>
  );
}
