import axios from 'axios'

let api = axios.create({
    headers : {
        'Client-ID' : 'y3731pticyzwop9lmg3h5tn8u6q1uv',
        "Authorization" : "Bearer sln5lnme6aidntk5s7viqccf1rjbn5"
    }
})

/*
CLIENT_ID = "y3731pticyzwop9lmg3h5tn8u6q1uv";
REDIRECT_URI = "http://localhost:3000/";
RESPONSE_TYPE = "token";

https://id.twitch.tv/oauth2/authorize?client_id=y3731pticyzwop9lmg3h5tn8u6q1uv&redirect_uri=http://localhost:3000/&response_type=token&scope=<space-separated list of scopes>
*/

export default api;